# The-Organizers
An opensource series.

With the following rules


1) Each story ends with a reset. End your "reset" with the below 2 lines. If you do not, it will be assumed your story is incomplete. 
    
    $ sudo reset
    $ Password:
  

2) The ability to reset cannot be destroyed.

3) The computer began recording at 2015-03-19 02:59:51 Zulu. 

4) The title of your "reset" should follow the format 

    The-Organizers-Reset-[the number of your github fork]

